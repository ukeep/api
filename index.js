const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const Sentry = require("@sentry/node");
const cors = require("cors");

const config = require("./config");
const lib = require("./lib");
const errorApi = lib.errorApi;
const logger = lib.loggerApi;
const errorController = lib.errorController;

const AuthenticationController = require("./controllers/authentication");
const UserController = require("./controllers/users");
const SafeController = require("./controllers/safe");


/**
 * Serve apiDocs dependencies
 */

app.use(express.static("public"));

/**
 * Initialize Sentry
 * more: https://docs.sentry.io/platforms/node/express/
 */

Sentry.init({
	dsn: config.services.sentry,
	release: "0.1",
	environment: "dev"
});

app.use(Sentry.Handlers.requestHandler());

/**
 * Enable CORS request
 */

app.use(cors()); 

/**
 * Log all requests
 */

app.use((req, res, next) => {

	const remoteAddress = (req.header("X-Forwarded-For") || "").split(",").pop() ||
		req.connection.remoteAddress ||
		req.socket.remoteAddress ||
		req.connection.socket.remoteAddress;

	const remoteUser = req.header("Authorization");
	const userAgent = req.header("User-Agent");

	// ::1 - alpha_f5ad77e32e9126fffa981d218a4ddd1d36fa692b [22/Nov/2018:13:50:38 +0000] "GET / HTTP/1.1" 404 59 "-" "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:63.0) Gecko/20100101 Firefox/63.0"
	logger.info([
		remoteAddress,
		remoteUser,
		("\"" + req.method + " " + req.originalUrl + "\""),
		"\"" + userAgent + "\""
	].join(" "));

	return next();
});

/**
 *	Authenticate requests
 */

app.all("*", AuthenticationController);

/**
 *	Parse body on certain queries
 */

/**
 * Apply to request where Content-Type is set as "application/octet-stream" (binary)
 * This way, req.body is a buffer (useful for UsercontentModel)
 * More at https://github.com/expressjs/body-parser/blob/master/README.md#bodyparserrawoptions
 */
app.use(bodyParser.raw({
	type: "application/octet-stream",
	limit: "10mb"
}));

/**
 * Parse request with Content-Type is set as "application/x-www-form-urlencoded"
 * This allow us to use req.body.params :)
 * More at https://github.com/expressjs/body-parser/blob/master/README.md#bodyparserrawoptions
 */
app.use(bodyParser.urlencoded({
	extended: true,
	type: "application/x-www-form-urlencoded"
}));

//app.use(bodyParser.json());

/**
 *	Surcharge express with a response.success method
 */

app.response.success = function (json) {

	if (typeof json !== "object")
		throw errorApi.badRequest("Invalid object receive", "Unable to use res.success, not a json.");

	json.message = (typeof json.message == "string" ? json.message : "succeeded");
	json.statusCode = (typeof json.statusCode == "number" ? json.statusCode : 200);

	this.status(json.statusCode);
	this.json(json);
};

/**
 * Create an instance of the express Router
 */

let router = express.Router();

/**
 *	Sanity check route
 */

router.get("/", (req, res) => {
	return res.success({
		message: "GitArchive API ready - (Version: " + config.server.version + ")"
	});
});

/**
 * @api {get} /user/:user_id	Get user informations
 * @apiGroup Users
 *
 * @apiName GetUser
 * @apiVersion 1.0.0
 * @apiParam {Number} user_id	User id
 *
 * @apiSuccess {Number} id	User id
 * @apiSuccess {String} login	User login
 * @apiSuccess {String} email	User email
 *
 * @apiSuccessExample Success-Response:
 *		HTTP/1.1 200 OK
 *		{
 *			"id": 1,
 * 			"login": "tstark",
 * 			"email": "tony@stark.com"
 *		}
 *
 * @apiError UserNotFound The id of the User was not found.
 *
 * @apiErrorExample Error-Response:
 * 		HTTP/1.1 404 Not Found
 *		{
 *			"error": "user_not_found"
 *		}
 */
router.get("/user/", UserController.getInfo);

/**
 * @api {post} /user/:user_id	Create an user
 * @apiGroup Users
 *
 * @apiName CreateUser
 * @apiVersion 1.0.0
 * @apiParam {Number} user_id	User id
 *
 * @apiSuccess {Number} id	User id
 * @apiSuccess {String} login	User login
 * @apiSuccess {String} email	User email
 *
 * @apiSuccessExample Success-Response:
 *		HTTP/1.1 200 OK
 *		{
 *			"id": 1,
 * 			"login": "tstark",
 * 			"email": "tony@stark.com"
 *		}
 *
 * @apiError UserNotFound The id of the User was not found.
 *
 * @apiErrorExample Error-Response:
 * 		HTTP/1.1 404 Not Found
 *		{
 *			"error": "user_not_found"
 *		}
 */
router.put("/user/:user_id/settings", UserController.getInfo);

/**
 * @api {patch} /user/:user_id	Update specified user
 * @apiGroup Users
 *
 * @apiName UpdateUser
 * @apiVersion 1.0.0
 * @apiParam {Number} user_id	User id
 *
 * @apiSuccess {Number} id	User id
 * @apiSuccess {String} login	User login
 * @apiSuccess {String} email	User email
 *
 * @apiSuccessExample Success-Response:
 *		HTTP/1.1 200 OK
 *		{
 *			"id": 1,
 * 			"login": "tstark",
 * 			"email": "tony@stark.com"
 *		}
 *
 * @apiError UserNotFound The id of the User was not found.
 *
 * @apiErrorExample Error-Response:
 * 		HTTP/1.1 404 Not Found
 *		{
 *			"error": "user_not_found"
 *		}
 */
router.put("/user/:user_id/settings", UserController.getInfo);

/**
 * @api {delete} /user/:user_id	Delete specified user
 * @apiGroup Users
 *
 * @apiName DeleteUser
 * @apiVersion 1.0.0
 * @apiParam {Number} user_id	User id
 *
 * @apiSuccess {Number} id	User id
 * @apiSuccess {String} login	User login
 * @apiSuccess {String} email	User email
 *
 * @apiSuccessExample Success-Response:
 *		HTTP/1.1 200 OK
 *		{
 *			"id": 1,
 * 			"login": "tstark",
 * 			"email": "tony@stark.com"
 *		}
 *
 * @apiError UserNotFound The id of the User was not found.
 *
 * @apiErrorExample Error-Response:
 * 		HTTP/1.1 404 Not Found
 *		{
 *			"error": "user_not_found"
 *		}
 */
router.delete("/user/:user_id", UserController.getInfo);

/**
 * Safe relative routes
 */

/**
 * @api {get} /safe	Get safe
 * @apiGroup Safe
 *
 * @apiName GetSafe
 * @apiVersion 1.0.0
 * @apiParam {Number} safe_id Safe id
 *
 * @apiSuccess {Number} id Safe id
 *
 * @apiSuccessExample Success-Response:
 *		HTTP/1.1 200 OK
 *		{
 *
 *		}
 *
 * @apiError SafeNotFound The id of the safe was not found.
 *
 * @apiErrorExample Error-Response:
 * 		HTTP/1.1 404 Not Found
 *		{
 *			"error": "safe_not_found"
 *		}
 */
router.get("/safe", SafeController.getList);

/**
 * Safe relative routes
 */

/**
 * @api {get} /safe/:safe_id	Get safe
 * @apiGroup Safe
 *
 * @apiName GetSafe
 * @apiVersion 1.0.0
 * @apiParam {Number} safe_id Safe id
 *
 * @apiSuccess {Number} id Safe id
 *
 * @apiSuccessExample Success-Response:
 *		HTTP/1.1 200 OK
 *		{
 *
 *		}
 *
 * @apiError SafeNotFound The id of the safe was not found.
 *
 * @apiErrorExample Error-Response:
 * 		HTTP/1.1 404 Not Found
 *		{
 *			"error": "safe_not_found"
 *		}
 */
//router.get("/safe/:safe_id", SafeController.getInfo);

/**
 * @api {delete} /safe/:safe_id	Delete safe
 * @apiGroup Safe
 *
 * @apiName DeleteSafe
 * @apiVersion 1.0.0
 * @apiParam {Number} safe_id Safe id
 *
 * @apiSuccess {Number} id Safe id
 *
 * @apiSuccessExample Success-Response:
 *		HTTP/1.1 200 OK
 *		{
 *
 *		}
 *
 * @apiError SafeNotFound The id of the safe was not found.
 *
 * @apiErrorExample Error-Response:
 * 		HTTP/1.1 404 Not Found
 *		{
 *			"error": "safe_not_found"
 *		}
 */
//router.delete("/safe/:safe_id", SafeController.getInfo);


/**
 * Archive relative routes
 */

/**
 * @api {get} /archive/:archive_id	Get archive
 * @apiGroup Archive
 *
 * @apiName GetArchive
 * @apiVersion 1.0.0
 * @apiParam {Number} archive_id Archive id
 *
 * @apiSuccess {Number} id Archive id
 *
 * @apiSuccessExample Success-Response:
 *		HTTP/1.1 200 OK
 *		{
 *			"id": 1,
 * 			"login": "tstark",
 * 			"email": "tony@stark.com"
 *		}
 *
 * @apiError ArchiveNotFound The id of the archive was not found.
 *
 * @apiErrorExample Error-Response:
 * 		HTTP/1.1 404 Not Found
 *		{
 *			"error": "archive_not_found"
 *		}
 */
router.get("/archive/:archive_id", UserController.getInfo);

/**
 * @api {delete} /archive/:archive_id	Delete archive
 * @apiGroup Archive
 *
 * @apiName DeleteArchive
 * @apiVersion 1.0.0
 * @apiParam {Number} archive_id Archive id
 *
 * @apiSuccess {Number} id Archive id
 *
 * @apiSuccessExample Success-Response:
 *		HTTP/1.1 200 OK
 *		{
 *
 *		}
 *
 * @apiError ArchiveNotFound The id of the archive was not found.
 *
 * @apiErrorExample Error-Response:
 * 		HTTP/1.1 404 Not Found
 *		{
 *			"error": "archive_not_found"
 *		}
 */
router.delete("/archive/:archive_id", UserController.getInfo);


/**
 *	Config
 */

app.set("port", config.server.port);

// Prefix routes
app.use("/v" + config.server.version, router);
app.listen(app.get("port"));

logger.info("Server started and connected ! API listen on port: " + app.get("port"));

/**
 * Error handling
 */

/**
 * 404 responses
 * more: expressjs.com/en/starter/faq.html#how-do-i-handle-404-responses
 */
app.use(() => {
	throw errorApi.notFound("Not found");
});

/**
 * Sentry error handler must be before any other error middleware
 * more: https://docs.sentry.io/platforms/node/express/
 */
app.use(Sentry.Handlers.errorHandler());

// Error handler
app.use(errorController);
