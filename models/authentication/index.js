//const Database	= require("../../lib/database");

// TMP
const config = require("../../config");

module.exports = {

	/*
	 *	Check if API key send is valid
	 *	Return all infos
	 */
	getApiKeyInfo: (apiKey) => {

		return new Promise((resolve, reject) => {

			/*
			 * const sqlQuery = "SELECT pk, api_key, email, role"
			 * 	+ " " + "FROM api_keys"
			 * 	+ " " + "WHERE api_key = $1";
			 * return Database.oneOrNone(sqlQuery, [apiKey])
			 * 	.then(dbResult => {
			 * 		if (dbResult)
			 * 			return resolve(dbResult);
			 * 		return reject("Invalid api key provided");
			 * 	})
			 * 	.catch(Database.lib.handleErrors);
			 */

			// TMP
			if (apiKey != config.server.token)
				return reject("Invalid api key provided");
			return resolve({
				api_key: apiKey
			});
		});
	}
};
