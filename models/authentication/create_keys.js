const crypto = require("crypto");

const values = ["user1"];

for (let i = 0; i < values.length ; i++) {
	const value = values[i];

	const salt = "@ukeep.com";
	const hash = crypto.createHmac("sha1", salt).update(value, "utf8").digest("hex");

	console.log("\"" + hash + "\", ");
}
