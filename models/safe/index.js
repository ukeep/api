const lib = require("../../lib");
const config = require("../../config");
const providers = lib.providers;

module.exports = {

	/*
	 * Get safe list
	 */
	getList: () => {
		return providers.onlineApi.safe(config.providers.online).getList();
	}
};
