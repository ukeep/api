const lib = require("../../lib");
const config = require("../../config");
const providers = lib.providers;

module.exports = {

	/*
	 * Get user infos
	 */
	getInfos: async () => {

		return await providers.onlineApi.user(config.providers.online).getInfos();
	}
};
