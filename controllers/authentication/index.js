const lib = require("../../lib");
const errorApi = lib.errorApi;

const AuthenticationModel = require("../../models/authentication");

module.exports = async (req, res, next) => {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	res.header("Access-Control-Allow-Methods", "PATCH,POST,GET,DELETE,OPTIONS");

	const requestAuthHeader = (req.headers["authorization"] ? req.headers["authorization"] : null);

	if (!requestAuthHeader) {
		res.header("WWW-Authenticate", "Basic realm=\"Please provide a valid API key as username (no password required).\"");

		return next(errorApi.unauthorized("Authentication required."));
	}

	const requestAuth = requestAuthHeader.split(" ");

	if (requestAuth.length !== 2) {
		return next(errorApi.unauthorized("Authentication failed."));
	}

	const requestAuthScheme = requestAuth[0];
	const requestAuthCredentials = Buffer.from(requestAuth[1], "base64").toString("utf-8");

	const requestAuthApikey = requestAuthCredentials.split(":")[0];

	if (requestAuthScheme !== "Basic") {
		return next(errorApi.unauthorized("Authentication failed."));
	}

	/*
	 * Check if api key receive
	 * Load api_key information in local var
	 */

	await AuthenticationModel.getApiKeyInfo(requestAuthApikey)
		.then(apiKeyInfo => {
			res.locals = apiKeyInfo;
		})
		.catch((err) => {
			return next(errorApi.unauthorized("Authentication failed.", err));
		});

	if (req.method === "OPTIONS")
		return res.status(200).end();
	return next();
};
