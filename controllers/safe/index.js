const getSafeList = require("./list");

/*
 * Safe controller
 */

const Safe = {
	getList: getSafeList
};

module.exports = Safe;