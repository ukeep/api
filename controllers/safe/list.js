const SafeModel = require("../../models/safe");

module.exports = (req, res, next) => {

	// Get safe info from remote API
	SafeModel.getList()
		.then(SafeList => {
			return res.success(SafeList);
		})
		.catch(next);
};
