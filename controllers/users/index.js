const getUserInfos = require("./infos");

/*
 * Users controller
 */

const Users = {
	getInfo: getUserInfos
};

module.exports = Users;
