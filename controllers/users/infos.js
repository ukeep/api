const UsersModel = require("../../models/users");

module.exports =  (req, res, next) => {

	// Get user info from remote API
	UsersModel.getInfos()
		.then(UserInfos => {
			return res.success(UserInfos);
		})
		.catch(next);
};
