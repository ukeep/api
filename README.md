Start the application
------

Clone git repository and go in it:
```
git clone https://gitlab.com/ukeep/api
cd api
```

Generate server token authentication (used by client), need later:
```
node models/authentication/create_keys.js
```

Clone `config.js.sample` and fill information:
```
cp config.js.sample config.js
vi config.js
````

You need to provide:
- providers information:
  * api version
  * hostname of api
  * remote port
  * secret token

- local server settings:
  * hostname (`127.0.0.1`)
  * version (1)
  * local port: (8080)
  * token authentication generated previously
 
- sentry token, login an go to [sentry](`https://sentry.io/ukeep/ukeep-api/getting-started/node-express/`)
- local or remote db credentials
  * hostname
  * port (default postgresql port)
  * username
  * password

Download dependencies and launch project:
```
npm i
node index.js
```

If everything works, this must display:
```
Server started and connected !
```