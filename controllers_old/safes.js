// TODO check if safe is correct
const async	= require("async");
const fct	= require("./utils.js");
const config	= require("../../config.json");

module.exports = {
	// List safes
	list: (callback) => {
		fct.GET(config.url + "/api/v1/storage/c14/safe", (err, result) => {
			if (err && err.error)
				return callback("Can't list safes: " + err.error);
			if (err)
				return callback(err);
			return callback(null, result);
		});
	},

	// Create safe
	create: (name, description, callback) => {
		fct.POST(config.url + "/api/v1/storage/c14/safe", {
			"name": name,
			"description": description
		}, (err, result) => {
			if (err && err.error)
				return callback("Can't create safe: " + err.error);
			if (err)
				return callback(err);
			return callback(null, result);
		});
	},

	// Edit info on safe
	getInfo: (safeId, callback) => {
		fct.GET(config.url + "/api/v1/storage/c14/safe/" + safeId, (err, result) => {
			if (err && err.error)
				return callback("Can't get info for safe ID (" + safeId + "): " + err.error);
			if (err)
				return callback(err);
			return callback(null, result);
		});
	},
	
	// Edit a particular safe
	edit: (safeId, name, description, callback) => {
		fct.PATCH(config.url + "/api/v1/storage/c14/safe/" + safeId, {
			"name": name,
			"description": description
		}, (err, result) => {
			if (err && err.error)
				return callback("Can't edit info for safe ID (" + safeId + "): " + err.error);
			if (err)
				return callback(err);
			return callback(null, result);
		});
	},
	
	// Delete a particular safe
	Delete: (safeId, callback) => {
		fct.DEL(config.url + "/api/v1/storage/c14/safe/" + safeId, (err, result) => {
			if (err && err.error)
				return callback("Can't delete safe ID (" + safeId + "): " + err.error);
			if (err)
				return callback(err);
			return callback(null, result);
		});
	}
};
