const request	= require("request");
const config	= require("../../config.json");
const Store	= require("electron-store");
const store	= new Store({
	name: config.storeName,
	encryptionKey: config.storeEncryptionKey // Config file is crypt ;-)
});

module.exports = {
	POST: (url, form, callback) => {
		const auth = store.get("auth");
		request.post({
			url: url,
			form: form,
			headers: {
				"Authorization": "Bearer " + auth.accessToken,
				"Content-Type": "application/json"
			}
		}, function (err, res, body) {
			// Try to parse JSON return by remote url
			try {
				var data = JSON.parse(body);
			} catch (e) {
				return callback(e);
			}
			// print error
			if (err)
				return callback(err);
			else if (res.statusCode !== 200 && res.statusCode !== 201 && res.statusCode !== 202)
				return callback(data);
			return callback(null, data);
		});
	},
	
	GET: (url, callback) => {
		const auth = store.get("auth");
		request({
			url: url,
			headers: {
				"Authorization": "Bearer " + auth.accessToken,
				"Content-Type": "application/json"
			}
		}, function (err, res, body) {
			// Try to parse JSON return by remote url
			try {
				var data = JSON.parse(body);
			} catch (e) {
				return callback(e);
			}
			// print error
			if (err)
				return callback(err);
			else if (res.statusCode !== 200)
				return callback(data);
			return callback(null, data);
		});
	},
	
	DEL: (url, callback) => {
		const auth = store.get("auth");
		request.del({
			url: url,
			headers: {
				"Authorization": "Bearer " + auth.accessToken,
				"Content-Type": "application/json"
			}
		}, function (err, res, body) {
			// print error
			if (err)
				return callback(err);
			if (res.statusCode === 204)
				return callback(null, true);
	
			// Try to parse JSON return by remote url
			try {
				var data = JSON.parse(body);
			} catch (e) {
				return callback(e);
			}
			return callback(data);
		});
	},
	
	PATCH: (url, form, callback) => {
		const auth = store.get("auth");
		request.patch({
			url: url,
			form: form,
			headers: {
				"Authorization": "Bearer " + auth.accessToken,
				"Content-Type": "application/json"
			}
		}, function (err, res, body) {
			// print error
			if (err)
				return callback(err);
			if (res.statusCode === 204)
				return callback(null, true);
	
			// Try to parse JSON return by remote url
			try {
				var data = JSON.parse(body);
			} catch (e) {
				return callback(e);
			}
			return callback(data);
		});
	}
};
