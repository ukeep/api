const request	= require("request");
const config	= require("../../config.json");
const fct	= require("./utils");

module.exports = {
	// Get user info, used to valid apiKey
	userInfo: (callback) => {
		fct.GET(config.url + "/api/v1/user", (err, result) => {
			if (err && err.error && err.error_description)
				return callback(err.error_description);
			return callback(null, result);
		});
	},
	// Get list of all protocols
	getProtocols: (callback) => {
		fct.GET(config.url + "/api/v1/storage/c14/protocol", (err, result) => {
			if (err && err.error)
				return callback(err.error);
			return callback(null, result);
		});
	},

	// Get list of all protocols
	getPlatforms: (callback) => {
		fct.GET(config.url + "/api/v1/storage/c14/platform", (err, result) => {
			if (err && err.error)
				return callback(err.error);
			return callback(null, result);
		});
	},

	// Check  if valid protocol
	isValidProtocol: (protocol, callback) => {
		fct.getPlatforms((err, result) => {
			if (err)
				return callback(err);
			for (let i = 0; i < result.length; i++) {
				if (result[i].name.toLowerCase() === protocol.toLowerCase())
					return callback(null, true);
			}
			return callback("invalid protocol.");
		});
	},

	// Check  if valid platform
	isValidPlatform: (protocol, callback) => {
		fct.getPlatforms((err, result) => {
			if (err)
				return callback(err);
			for (let i = 0; i < result.length; i++) {
				if (result[i].id === protocol.toString())
					return callback(null, true);
			}
			return callback("invalid platform.");
		});
	}
};
