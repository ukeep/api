const async	= require("async");
const fct	= require("./utils.js");
const config	= require("../../config.json");

module.exports = {
	// Get ssh key of user
	list: (callback) => {
		fct.GET(config.url + "/api/v1/user/key/ssh", (err, result) => {
			if (err && err.error)
				return callback("Can't get ssh key: " + err.error);
			if (err)
				return callback(err);
			return callback(null, result);
		});
	},
	// get info of a particular ssh key
	getInfo: (keyId, callback) => {
		if (!keyId)
			return callback("No key id provided.");

		fct.GET(config.url + "/api/v1/user/key/ssh/" + keyId, (err, result) => {
			if (err && err.error)
				return callback("Can't get key Id info (" + keyId + "): " + err.error);
			if (err)
				return callback(err);
			return callback(null, result);
		});
	},
	// Add ssh key
	add: (description, content, callback) => {
		if (!description)
			return callback("No description provided.");
		if (!content)
			return callback("No content provided.");

		fct.POST(config.url + "/api/v1/user/key/ssh", {
			content: content,
			description: description
		}, (err, result) => {
			if (err && err.error)
				return callback("Can't add ssh key: " + err.error);
			if (err)
				return callback(err);
			return callback(null, result);
		});
	},

};
