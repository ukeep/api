// TODO check if safe is correct
const async	= require("async");
const fct	= require("./utils.js");
const config	= require("../../config.json");

module.exports = {
	// Get jobs list for specifiv archive and safe id
	list: (safeId, archiveId, callback) => {
		if (!safeId)
			return callback("No safe id provided.");
		if (!archiveId)
			return callback("No archive id provided.");

		fct.GET(config.url + "/api/v1/storage/c14/safe/" + safeId + "/archive/" + archiveId + "/job", (err, result) => {
			if (err && err.error)
				return callback("Can't get jobs list for archive ID (" + archiveId + "): " + err.error);
			if (err)
				return callback(err);
			return callback(null, result);
		});
	},
	// get info of particular job id
	getInfo: (safeId, archiveId, jobId, callback) => {
		if (!safeId)
			return callback("No safe id provided.");
		if (!archiveId)
			return callback("No archive id provided.");
		if (!jobId)
			return callback("No job id provided.");

		fct.GET(config.url + "/api/v1/storage/c14/safe/" + safeId + "/archive/" + archiveId + "/job/" + jobId, (err, result) => {
			if (err && err.error)
				return callback("Can't get jobs list for archive ID (" + archiveId + "): " + err.error);
			if (err)
				return callback(err);
			return callback(null, result);
		});
	}
};
