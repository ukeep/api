const http = require("http");
const https = require("https");

const errorApi = require("../api/error");

// Request options config
let RequestOptions = function (config, path = null) {

	const _path = (typeof path == "string") ? path : "";
	let options = {};

	const basicAuthKey = config.token;
	options.hostname = config.hostname;
	options.port = config.port;
	options.path = "/api/v" + config.version + _path;
	options.path_prefix = "/api/v" + config.version;
	options.headers = {
		Authorization: "Bearer" + " " + basicAuthKey
	};

	return options;
};


/*
 * User class
 * List, create, edit, delete user following api key send
 */

let User = {

	// Return user informations
	getInfos: function () {

		this.options.path = "/user";
		this.options.method = "GET";
		this.options.onFailureMessage = "Unable to get current user infos.";

		return fetchJSON(this.options);
	}

};

/*
 * Safe class
 * List, create, edit, delete safes following api key send
 */

let Safe = {

	// Return safe list
	getList: function () {

		this.options.path = "/storage/c14/safe";
		this.options.method = "GET";
		this.options.onFailureMessage = "Unable to get current safe list.";
	
		return fetchJSON(this.options);
	}

};

/*
 * fetchJSON to make API request
 */

let fetchJSON = (options) => {

	return new Promise((resolve, reject) => {

		if (options.path_prefix) {
			options.path = options.path_prefix + options.path;
		}
		const port = options.port == 443 ? https : http;
		const req = port.request(options, (responseApi) => {

			// const { statusCode } = responseApi;
			let body = [];
			responseApi.on("data", (chunk) => {
				body.push(chunk);
			});
			responseApi.on("end", () => {

				// Catch request execution and check status code receive
				try {
					body = Buffer.concat(body).toString();
					let response = JSON.parse(body);

					if (responseApi.statusCode && (responseApi.statusCode >= 200 && responseApi.statusCode < 300)) {
						return resolve(response);
					}
					
					return reject(errorApi.badImplementation("Error from reponse received."));
				}
				catch (err) {
					return reject(errorApi.badImplementation("Failed to parse online API response as JSON.", err));
				}
			});
		});

		if (typeof options.body !== "undefined") {
			req.write(options.body);
		}

		req.on("error", (err) => {
			return reject(errorApi.badImplementation("Failed to contact online API.", err));
		});
		req.end();
	});
};

/*
 * Create array function by data type
 */

module.exports = {

	// User array function
	user: (config, UserItem={}) => {

		for (let key in User) {
			UserItem[key] = User[key];
		}

		UserItem.options = RequestOptions(config);
		return UserItem;
	},

	// Safe array function
	safe: (config, SafeItem={}) => {

		for (let key in Safe) {
			SafeItem[key] = Safe[key];
		}

		SafeItem.options = RequestOptions(config);
		return SafeItem;
	}
};
