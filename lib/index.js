const errorApi		= require("./api/error");
const errorController	= require("./controllers/error");
const loggerApi		= require("./api/logger");
const onlineApi		= require("./providers/online");

module.exports = {
	errorApi: errorApi,
	errorController: errorController,
	loggerApi: loggerApi,
	providers: {
		onlineApi: onlineApi
	}
};
