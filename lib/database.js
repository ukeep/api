// or any other Promise/A+ compatible library;
const promise		= require("bluebird");
const IS_PRODUCTION	= (process.env.NODE_ENV == "production");
const config	    = require("../config");

const lib			= require("./index.js");
const errorApi		= lib.errorApi;

// overriding the default (ES6 Promise);
const initOptions = {
	promiseLib: promise
};
const pgp		= require("pg-promise")(initOptions);

/*
 * Database connection details;
 * ssl: allows basic connection to DB on dev
 */
const cn		= {
	host: config.services.db.hostname,
	port: config.services.db.port,
	database: config.services.db.database,
	user: config.services.db.username,
	password: config.services.db.password,
	ssl: !IS_PRODUCTION
};

// Instantiate connection
const db = pgp(cn);

// Surcharge db object witb a lib object...
const Database = Object.assign({

	lib: {

		// ...with a custom errors handler,
		handleErrors: (err) => {
			if (err.statusCode)
				throw err;
			else
				throw errorApi.notImplemented(err);
		},

		// ...with a custom query limit.
		sqlQueryLimit: (limit) => {
			const SQL_DEFAULT_LIMIT = 25;
			return (Number(limit) && limit > 0 && limit < SQL_DEFAULT_LIMIT * 10 ? limit : SQL_DEFAULT_LIMIT);
		},

		formatNumber: (int) => {
			return isNaN(int) ? 0 : int;
		},

		formatDate: (date) => {
			if (typeof date !== "undefined" && typeof date.getMonth == "function") {
				return date;
			}
			
			const dateObj = new Date(date);

			return isNaN(dateObj.getTime()) ? null : dateObj.toISOString();
		}
	}
}, db);

module.exports = Database;
