/*
 * All API Errors Handler
 */

const logger		= require("../api/logger");

module.exports = (err, req, res, next) => {

	/*
	 * When you add a custom error handler,
	 * we must delegate to the default Express error handler,
	 * when the headers have already been sent to the client.
	 * see: https://expressjs.com/en/guide/error-handling.html#the-default-error-handler
	 */

	if (res.headersSent) {
		return next(err);
	}

	/*
	 * Parsing the error received.
	 * Default json error object
	 */

	let error = {
		error: true,
		statusCode: (err.statusCode) ? err.statusCode : 500,
		message: (err.message) ? err.message : "That's all we know."
	};

	/*
	 * If error come from system instead of custom ErrorBoom
	 * we need to hide message to end user
	 */

	if (err instanceof Error) {
		error.data = error.message;
		error.message = "Internal server error please contact contact@ukeep.fr";
	}

	const remoteUser = req.header("Authorization");
	const userAgent = req.header("User-Agent");
	const remoteAddress = (req.header("X-Forwarded-For") || "").split(",").pop()
		|| req.connection.remoteAddress
		|| req.socket.remoteAddress
		|| req.connection.socket.remoteAddress;

	// Print one by one in terminal, request parameter and error messages
	logger.error([
		remoteAddress,
		remoteUser,
		("\"" + req.method + " " + req.originalUrl + "\""),
		error.statusCode,
		"\"" + userAgent + "\"",
		error.message,
		(error.data ? error.data : "")
	].join(" "));

	// Print stack trace of error
	console.error(err.stack || err);

	// error.data must be only for admin user (can tell critical information about the architecture)
	delete error.data;

	// Returning the error to the client.
	return res.status(error.statusCode).send(error);
};
